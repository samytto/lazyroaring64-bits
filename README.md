LazyHierarchical64-bitRoaringBitmap
=============

Hierarchical 64-bit version based on the Roaring bitmap's modal. Unlike Roaring, this scheme adopts 2 levels of indexes above containers. The first level stores recurrent high 32 bits of the integers, and the second level stores recurrent high 16 bits following the high 32 bits of the represented integers. As for Roaring, the remaining least 16 bits of the stored integers are maintained on the same container's modal in the last level of a LazyRoaing.

LazyRoaring adopts the copy-on-write strategy when performing unions, with which experiments shown great improvements when calculating OR logical operations between bitmaps : https://bitbucket.org/samytto/datastructures64-bitints.    

License
------
This code is licensed under Apache License, Version 2.0 (ASL2.0).

Usage
------

* Get java
* Get maven 2

* mvn compile will compile
* mvn test will run the unit tests
* mvn package will package in a jar (found in target)

Funding 
----------

This work was supported by NSERC grant number 26143.
