package org.hierarchicalroaringbitmap;


public class SecondLevelEntry implements Cloneable, Comparable<SecondLevelEntry> {
	
	private short value;
	private Container container;
	private boolean shared;
	
	public SecondLevelEntry(final int x) {
		value = (short)(x>>16);
		container = new ArrayContainer().add((short)x);
	}
	
	public SecondLevelEntry(final short a, final Container c) {
		value = a;
		container = c;
	}
	
	public void add(final short x) {
		container = container.add(x);
	}
	
	@Override
    public int hashCode() {
    	return value * 0xF0F0F0 + container.hashCode();
    }
	
	public static SecondLevelEntry or(final SecondLevelEntry e1, final SecondLevelEntry e2) {
		SecondLevelEntry sle = new SecondLevelEntry(e1.getValue(), e1.container.or(e2.container));
		return sle; 
	}
	
	public static SecondLevelEntry and(final SecondLevelEntry e1, final SecondLevelEntry e2) {
		SecondLevelEntry sle = new SecondLevelEntry(e1.getValue(), e1.container.and(e2.container));
		return sle; 
	}
	
	public short getValue() {
		return this.value;
	}
	
	public void setShared(final boolean b) {
		shared = b;
	}
	
	public boolean contains(final short x) {
		return container.contains(x);
	}
	
	public Container getContainer() {
		return this.container;		
	}
	
	@Override
    public boolean equals(Object o) {
        if (o instanceof SecondLevelEntry) {
            final SecondLevelEntry sle = (SecondLevelEntry) o;
            if(sle.value!=this.value || !container.equals(sle.container))
            	return false;
            return true;
        }
        return false;
    }
	
	public boolean isShared(){
		return shared;
	}
	
	@Override
	public int compareTo(SecondLevelEntry o) {		
		return (value < o.value) ? -1 : ((value == o.value) ? 0 : 1);
	}
	
	@Override
    public SecondLevelEntry clone() {
        try {
            final SecondLevelEntry x = (SecondLevelEntry) super.clone();
            x.value=value;
            x.shared=shared;
            x.container=container.clone();
            return x;
        } catch (final CloneNotSupportedException e) {
            throw new RuntimeException("shouldn't happen with clone", e);
        }
    }
	
	public boolean remove(short x) {		
		if(shared)
			container=container.clone();
		int oldCard = container.getCardinality();
		Container c = container.remove(x);
		if(c.getCardinality()<oldCard)
			return true;
		return false;		
	}	
}
