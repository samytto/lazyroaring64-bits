package org.hierarchicalroaringbitmap;

import java.util.Arrays;


public class FirstLevelEntry implements Comparable<FirstLevelEntry>, Cloneable {
	private int value; 
	public SecondLevelEntry[] array;
	private int size=0;
	private boolean shared;
	
	
	@Override
    public FirstLevelEntry clone() {
        try {
            final FirstLevelEntry x = (FirstLevelEntry) super.clone();
            x.value=value;
            x.size=size;
            x.shared=shared;
            x.array = array.clone();
            return x;
        } catch (final CloneNotSupportedException e) {
            throw new RuntimeException("shouldn't happen with clone", e);
        }
    }
	
	public FirstLevelEntry(final long x) {	
		value = (int)(x>>32);
		array = new SecondLevelEntry[4];
		array[0] = new SecondLevelEntry((int)x);
		size++;
	}
	
	public int size(){
		return size;
	}
	
	public int getNbChilds(){
		return size;
	}
	
	public boolean isShared(){
		return shared;
	}
	
	public FirstLevelEntry(final int x, final int arrayLength) {
		value = x;
		array = new SecondLevelEntry[arrayLength];
	}
	
	public void add(final int x) {			
		int pos = UtilLazyRoaring64bits.binarySearch((short)(x>>16), array, 0, size);
		if(pos<0) {
			extend();
			insertNewSecdLvlEntry(x, -pos-1);
			size++;
		}			
		else
			array[pos].add((short)x);		
	}
	
	public void insertNewSecdLvlEntry(int x, int pos) {
    	System.arraycopy(array, pos, array, pos+1, size-pos);
    	array[pos]=new SecondLevelEntry(x);
    }

    /**
     * Extend the first level array.
     */
    private void extend() {
    	if(this.size>=this.array.length){    		
    		int newCapacity;
            if (this.array.length < 1024)
                newCapacity = 2 * (this.size+1);
            else 
                newCapacity = 5 * (this.size + 1) / 4;
            this.array = Arrays.copyOf(this.array, newCapacity);
    	}    	
    }
	
	public void setShared(final boolean shared) {
		this.shared = shared;
	}
	
	public static FirstLevelEntry or(final FirstLevelEntry e1, final FirstLevelEntry e2) {
		final int length1 = e1.size, length2 = e2.size, length = length1+length2;
		FirstLevelEntry fle = new FirstLevelEntry(e1.getValue(), length);
		int pos1=0, pos2=0, pos=0;
        int s1 = e1.array[pos1].getValue();
        int s2 = e2.array[pos2].getValue();
        
        do {
                if (s1 < s2) {
                    fle.array[pos]=e1.array[pos1];
                    fle.array[pos].setShared(true);
                    pos1++;
                    pos++;
                    if(pos1 == length1)
                        break;
                    s1 = e1.array[pos1].getValue();
                } else if (s1 > s2) {
                    fle.array[pos]=e2.array[pos2];
                    fle.array[pos].setShared(true);
                    pos2++;
                    pos++;
                    if (pos2 == length2) {
                        break;
                    }
                    s2 = e2.array[pos2].getValue();
                } else {
                    fle.array[pos]= SecondLevelEntry.or(e1.array[pos1],e2.array[pos2]);
                    pos1++;
                    pos2++;
                    pos++;
                    if ((pos1 == length1) || (pos2 == length2)) {
                        break;
                    }
                    s1 = e1.array[pos1].getValue();
                    s2 = e2.array[pos2].getValue();
                }
            } while(true);
        
        if (pos1 == length1) {
        	while(pos2<length2) {
        		fle.array[pos]=e2.array[pos2];
        		fle.array[pos].setShared(true);
        		pos++;
        		pos2++;
        	}
        } else if (pos2 == length2) 
        			while(pos1 < length1) {
        				fle.array[pos]=e1.array[pos1];
        				fle.array[pos].setShared(true);
        				pos++;
        				pos1++;
        			}        
        if(pos<length)
        	fle.array = Arrays.copyOf(fle.array, pos);
        fle.size=pos;
        return fle;
	}
	
	public static FirstLevelEntry and(final FirstLevelEntry e1, final FirstLevelEntry e2) {
		final int 	length1 = e1.size, length2 = e2.size, 
					length = length1<length2?length1:length2;
		FirstLevelEntry fle = new FirstLevelEntry(e1.getValue(), length);
		int pos1=0, pos2=0, pos=0;
        int s1 = e1.array[pos1].getValue();
        int s2 = e2.array[pos2].getValue();
        
        do {
                if (s1 < s2) {
                    pos1++;
                    if(pos1 == length1)
                        break;
                    s1 = e1.array[pos1].getValue();
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2)
                        break;
                    s2 = e2.array[pos2].getValue();
                } else {
                    fle.array[pos]= SecondLevelEntry.and(e1.array[pos1],e2.array[pos2]);
                    pos1++;
                    pos2++;
                    pos++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break;
                    s1 = e1.array[pos1].getValue();
                    s2 = e2.array[pos2].getValue();
                }
            } while(true);
               
        if(pos<length)
        	fle.array = Arrays.copyOf(fle.array, pos);
        fle.size=pos;
        return fle;
	}
	
	public boolean contains(final int x) {
		final int pos = UtilLazyRoaring64bits.binarySearch((short)(x>>16), array, 0, size);
		return pos<0 ? false : array[pos].contains((short)x);
	}
	
	public SecondLevelEntry[] getSecLevelArray(){
		return this.array;
	}
	
	@Override
    public int hashCode() {
		int secHashVal=0;
		for(int i=0; i<size; i++)
			secHashVal+=array[i].hashCode();
    	return value * 0xF0F0F0 + secHashVal;
    }
	
	public int getValue() {	
		return this.value;
	}			
	
	@Override
	public int compareTo(final FirstLevelEntry o) {		
		return value < o.value ? -1 : value == o.value ? 0 : 1;
	}
	
	@Override
    public boolean equals(Object o) {
        if (o instanceof FirstLevelEntry) {
            final FirstLevelEntry fle = (FirstLevelEntry) o;
            if(fle.value!=this.value)
            	return false;
            return Arrays.equals(array, fle.array);
        }
        return false;
    }
	
	/**
     * If present remove the specified integers (effectively, sets its bit
     * value to false).
     *
     * @param x integer value representing the index in a bitmap
     */
    public boolean remove(int x) {
        final int hb = x>>>16;
        final int i = getIndex(hb);
        if (i < 0)
            return false;
        if(array[i].isShared())
        	array[i]=array[i].clone();
        boolean b = array[i].remove((short) x);
        if (b && array[i].getContainer().getCardinality() == 0)
            removeAtIndex(i);
        return b;
    }
    
    /**
     * Removes the i th entry in the array by shifting the entries in ranges i+1 to size by
     * one position at left. The size field is decremented by one after that.
     * highLowContainer array is supposed to have at least one element.  
     * @param i the index of the entry to remove from the array.
     */
    public void removeAtIndex(int i) {
    	System.arraycopy(array, i+1, array, i, size-i-1);
    	size--;
    }
    
    public int getIndex(int hb) {
    	int pos = UtilLazyRoaring64bits.binarySearch(hb, array, 0, size);
    	return pos;
    }
}
