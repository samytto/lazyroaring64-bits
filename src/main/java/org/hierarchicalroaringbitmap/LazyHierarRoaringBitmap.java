/*
 * (c) Daniel Lemire, Owen Kaser, Samy Chambi, Jon Alvarado, Rory Graves, Björn Sperber
 * Licensed under the Apache License, Version 2.0.
 */

package org.hierarchicalroaringbitmap;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author Samy Chambi
 *
 */
/**
 * LazyHierarRoaringBitmap, a compressed alternative to the BitSet.
 * 
 * <pre>
 * {@code
 *      import org.hierarchicalroaringbitmap.*;
 *       
 *      //...
 *      
 *      LazyHierarRoaringBitmap rr = LazyHierarRoaringBitmap.bitmapOf(1,2,3,1000);
 *      LazyHierarRoaringBitmap rr2 = new LazyHierarRoaringBitmap();
 *      for(int k = 4000; k<4255;++k) rr2.add(k);
 *      LazyHierarRoaringBitmap rror = LazyHierarRoaringBitmap.or(rr, rr2);
 * }
 * </pre>
 *
 *
 * 
 */
public class LazyHierarRoaringBitmap implements Cloneable, Serializable, Iterable<Integer>, Externalizable, 
												ImmutableBitmapDataProvider {

    private static final long serialVersionUID = 6L;

    /**
     * Bitwise OR (union) operation. The provided bitmaps are *not*
     * modified. This operation is thread-safe as long as the provided
     * bitmaps remain unchanged.
     * 
     * If you have more than 2 bitmaps, consider using the
     * FastAggregation class.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     * 
     */
    public static LazyHierarRoaringBitmap or(final LazyHierarRoaringBitmap x1,
                                   final LazyHierarRoaringBitmap x2) {
    	if(x1.size==0)
    		return x2.clone();
    	if(x2.size==0)
    		return x1.clone();
    	final int length1 = x1.size, length2 = x2.size, length=length1+length2;
    	final LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap(length);
        int pos1=0, pos2=0, pos=0;
        int s1 = x1.highLowContainer[pos1].getValue();
        int s2 = x2.highLowContainer[pos2].getValue();        
        do {
                if (s1 < s2) {
                    answer.highLowContainer[pos]=x1.highLowContainer[pos1];
                    answer.highLowContainer[pos].setShared(true);
                    pos1++;
                    pos++;
                    if (pos1 == length1)
                        break;
                    s1 = x1.highLowContainer[pos1].getValue();
                } else if (s1 > s2) {
                    answer.highLowContainer[pos]=x2.highLowContainer[pos2];
                    answer.highLowContainer[pos].setShared(true);
                    pos2++;
                    pos++;
                    if (pos2 == length2)
                        break;
                    s2 = x2.highLowContainer[pos2].getValue();
                } else {
                    answer.highLowContainer[pos]= FirstLevelEntry.or(x1.highLowContainer[pos1],
                                x2.highLowContainer[pos2]);
                    pos1++;
                    pos2++;
                    pos++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break;
                    s1 = x1.highLowContainer[pos1].getValue();
                    s2 = x2.highLowContainer[pos2].getValue();
                }
            } while(true);
        
        if (pos1 == length1) {
        	while(pos2<length2) {
        		answer.highLowContainer[pos]=x2.highLowContainer[pos2];
        		answer.highLowContainer[pos].setShared(true);
        		pos++;
        		pos2++;
        		
        	}
        } else if (pos2 == length2) 
        			while(pos1 < length1) {
        				answer.highLowContainer[pos]=x1.highLowContainer[pos1];
        				answer.highLowContainer[pos].setShared(true);
        				pos++;
        				pos1++;
        			}
        if(pos<length) 
        	answer.highLowContainer = Arrays.copyOf(answer.highLowContainer, pos);        	
        answer.size=pos;
        return answer;
    }

    /*
     * Bitwise ANDNOT (difference) operation. The provided bitmaps are *not*
     * modified. This operation is thread-safe as long as the provided
     * bitmaps remain unchanged.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     *
   /* public static LazyHierarRoaringBitmap andNot(final LazyHierarRoaringBitmap x1,
                                       final LazyHierarRoaringBitmap x2) {
        final LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        int pos1 = 0, pos2 = 0;
        final int length1 = x1.highLowContainer.size(), length2 = x2.highLowContainer
                .size();
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = x1.highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);
            do {
                if (s1 < s2) {
                    answer.highLowContainer.appendCopy(
                            x1.highLowContainer, pos1);
                    pos1++;
                    if (pos1 == length1)
                        break main;
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = x1.highLowContainer
                            .getContainerAtIndex(pos1)
                            .andNot(x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0)
                        answer.highLowContainer.append(s1, c);
                    pos1++;
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break main;
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            } while (true);
        }
        if (pos2 == length2) {
            answer.highLowContainer.appendCopy(x1.highLowContainer, pos1, length1);
        }
        return answer;
    } */

    /**
     * Generate a bitmap with the specified values set to true. The provided
     * integers values don't have to be in sorted order, but it may be
     * preferable to sort them from a performance point of view.
     *
     * @param dat set values
     * @return a new bitmap
     */
    public static LazyHierarRoaringBitmap bitmapOf(final long... dat) {
        final LazyHierarRoaringBitmap ans = new LazyHierarRoaringBitmap();
        for (final long i : dat)
            ans.add(i);
        return ans;
    }

    /*
     * Complements the bits in the given range, from rangeStart (inclusive)
     * rangeEnd (exclusive). The given bitmap is unchanged.
     *
     * @param bm         bitmap being negated
     * @param rangeStart inclusive beginning of range
     * @param rangeEnd   exclusive ending of range
     * @return a new Bitmap
     *
    public static LazyHierarRoaringBitmap flip(LazyHierarRoaringBitmap bm,final int rangeStart, final int rangeEnd) {
        if (rangeStart >= rangeEnd) {
            return bm.clone();
        }

        LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        final short hbStart = Util.highbits(rangeStart);
        final short lbStart = Util.lowbits(rangeStart);
        final short hbLast = Util.highbits(rangeEnd - 1);
        final short lbLast = Util.lowbits(rangeEnd - 1);

        // copy the containers before the active area
        answer.highLowContainer.appendCopiesUntil(bm.highLowContainer, hbStart);

        int max = Util.toIntUnsigned(Util.maxLowBit());
        for (short hb = hbStart; hb <= hbLast; ++hb) {
            final int containerStart = (hb == hbStart) ? Util.toIntUnsigned(lbStart) : 0;
            final int containerLast = (hb == hbLast) ? Util.toIntUnsigned(lbLast) : max;

            final int i = bm.highLowContainer.getIndex(hb);
            final int j = answer.highLowContainer.getIndex(hb);
            assert j < 0;

            if (i >= 0) {
                Container c = bm.highLowContainer.getContainerAtIndex(i).not(containerStart, containerLast);
                if (c.getCardinality() > 0)
                    answer.highLowContainer.insertNewKeyValueAt(-j - 1, hb, c);

            } else { // *think* the range of ones must never be
                // empty.
                answer.highLowContainer.insertNewKeyValueAt(-j - 1, hb, Container.rangeOfOnes(
                                containerStart, containerLast)
                );
            }
        }
        // copy the containers after the active area.
        answer.highLowContainer.appendCopiesAfter(bm.highLowContainer, hbLast);

        return answer;
    } */

    /**
     * Bitwise AND (intersection) operation. The provided bitmaps are *not*
     * modified. This operation is thread-safe as long as the provided
     * bitmaps remain unchanged.
     * 
     * If you have more than 2 bitmaps, consider using the
     * FastAggregation class.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     */
    public static LazyHierarRoaringBitmap and(final LazyHierarRoaringBitmap x1,
                                   final LazyHierarRoaringBitmap x2) {
    	if(x1.size==0 || x2.size==0)
    		return new LazyHierarRoaringBitmap(1);
    	
    	final int length1 = x1.size, length2 = x2.size;
    	final int length = length1<length2 ? length1 : length2;
    	final LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap(length);
        int pos1=0, pos2=0, pos=0;
        int s1 = x1.highLowContainer[pos1].getValue();
        int s2 = x2.highLowContainer[pos2].getValue();
        
        do {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1)
                        break;
                    s1 = x1.highLowContainer[pos1].getValue();
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2)
                        break;
                    s2 = x2.highLowContainer[pos2].getValue();
                } else {
                    answer.highLowContainer[pos]= FirstLevelEntry.and(x1.highLowContainer[pos1],
                                x2.highLowContainer[pos2]);
                    pos1++;
                    pos2++;
                    pos++;
                    if (pos1 == length1 || pos2 == length2)
                        break;
                    s1 = x1.highLowContainer[pos1].getValue();
                    s2 = x2.highLowContainer[pos2].getValue();
                }
            } while(true);                        
        if(pos<length)
        	answer.highLowContainer = Arrays.copyOf(answer.highLowContainer, pos);
        answer.size=pos;
        return answer;
    }

    /*
     * Rank returns the number of integers that are smaller or equal to x (Rank(infinity) would be GetCardinality()).
     * @param x upper limit
     *
     * @return the rank
     
    public int rank(int x) {
        int size = 0;
        int xhigh = Util.highbits(x);
        
        for (int i = 0; i < this.highLowContainer.size(); i++) {
            short key =  this.highLowContainer.getKeyAtIndex(i);
            if( key < xhigh )      
              size += this.highLowContainer.getContainerAtIndex(i).getCardinality();
            else 
                return size + this.highLowContainer.getContainerAtIndex(i).rank(Util.lowbits(x));
        }
        return size;
    }*/
    

    /*
     * Return the jth value stored in this bitmap.
     * 
     * @param j index of the value 
     *
     * @return the value
     
    public int select(int j)
        int leftover = j;
        for (int i = 0; i < this.highLowContainer.length; i++) {
            Container c = this.highLowContainer.getContainerAtIndex(i);
            int thiscard = c.getCardinality();
            if(thiscard > leftover) {
                int keycontrib = this.highLowContainer.getKeyAtIndex(i)<<16;
                int lowcontrib = Util.toIntUnsigned(c.select(leftover));
                return  lowcontrib + keycontrib;
            }
            leftover -= thiscard;
        }
        throw new IllegalArgumentException("select "+j+" when the cardinality is "+this.getCardinality());
    }
*/
    
    /**
     * Bitwise XOR (symmetric difference) operation. The provided bitmaps
     * are *not* modified. This operation is thread-safe as long as the
     * provided bitmaps remain unchanged.
     * 
     * If you have more than 2 bitmaps, consider using the
     * FastAggregation class.
     *
     * @param x1 first bitmap
     * @param x2 other bitmap
     * @return result of the operation
     */
  /*  public static LazyHierarRoaringBitmap xor(final LazyHierarRoaringBitmap x1, final LazyHierarRoaringBitmap x2) {
        final LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        int pos1 = 0, pos2 = 0;
        final int length1 = x1.highLowContainer.size(), length2 = x2.highLowContainer.size();

        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = x1.highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);

            while (true) {
                if (s1 < s2) {
                    answer.highLowContainer.appendCopy(x1.highLowContainer, pos1);
                    pos1++;
                    if (pos1 == length1) {
                        break main;
                    }
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    answer.highLowContainer.appendCopy(x2.highLowContainer, pos2);
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = x1.highLowContainer.getContainerAtIndex(pos1).xor(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0)
                        answer.highLowContainer.append(s1, c);
                    pos1++;
                    pos2++;
                    if ((pos1 == length1)
                            || (pos2 == length2)) {
                        break main;
                    }
                    s1 = x1.highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            }
        }
        if (pos1 == length1) {
            answer.highLowContainer.appendCopy(x2.highLowContainer, pos2, length2);
        } else if (pos2 == length2) {
            answer.highLowContainer.appendCopy(x1.highLowContainer, pos1, length1);
        }

        return answer;
    }*/

    FirstLevelEntry[] highLowContainer;
    int size=0;    
    
    public int size(){
    	return size;
    }
    
    /**
     * Get the first level array's size.
     */
    public int getFisrtLevelIndexSize() {
    	return highLowContainer.length;
    }

    /**
     * Create an empty bitmap
     */
    public LazyHierarRoaringBitmap() {  
    	highLowContainer = new FirstLevelEntry[4];
    }
    
    public LazyHierarRoaringBitmap(int firstLevelSize){
    	highLowContainer = new FirstLevelEntry[firstLevelSize];
    }        

    /**
     * set the value to "true", whether it already appears or not.
     *
     * @param x integer value
     */
    public void add(final long x) {
    		int pos = UtilLazyRoaring64bits.binarySearch((int)(x>>32), highLowContainer, 0, size);
    		if(pos<0) {
    			extend();
    			insertNewFirstLvlEntry(x, -pos-1);
    			++size;
    		}
    		else //if chunk exists yet.
    			highLowContainer[pos].add((int) x);
    	}    
    
    public void insertNewFirstLvlEntry(long x, int pos) {
    	System.arraycopy(highLowContainer, pos, highLowContainer, pos+1, size-pos);
    	highLowContainer[pos]=new FirstLevelEntry(x);
    }

    /**
     * Extend the first level array.
     */
    private void extend() {
    	if(this.size>=this.highLowContainer.length){    		
    		int newCapacity;
            if (this.highLowContainer.length < 1024)
                newCapacity = 2 * (this.size+1);
            else 
                newCapacity = 5 * (this.size + 1) / 4;
            this.highLowContainer = Arrays.copyOf(this.highLowContainer, newCapacity);
    	}    	
    }
    
    /**
     * In-place bitwise AND (intersection) operation. The current bitmap is
     * modified.
     *
     * @param x2 other bitmap
     */
   /* public void and(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();
                
                 // TODO: This could be optimized quite a bit when one bitmap is
                 // much smaller than the other one.
                 
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);
            do {
                if (s1 < s2) {
                    highLowContainer.removeAtIndex(pos1);
                    --length1;
                    if (pos1 == length1)
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2)
                        break main;
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = highLowContainer.getContainerAtIndex(pos1).iand(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0) {
                        this.highLowContainer.setContainerAtIndex(pos1, c);
                        pos1++;
                    } else {
                        highLowContainer.removeAtIndex(pos1);
                        --length1;
                    }
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            } while (true);
        }
        highLowContainer.resize(pos1);
    } */
    
    /**
     * In-place bitwise ANDNOT (difference) operation. The current bitmap is
     * modified.
     *
     * @param x2 other bitmap
     */
  /*  public void andNot(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);
            do {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1)
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = highLowContainer.getContainerAtIndex(pos1).iandNot(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0) {
                        this.highLowContainer.setContainerAtIndex(pos1, c);
                        pos1++;
                    } else {
                        highLowContainer.removeAtIndex(pos1);
                        --length1;
                    }
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2))
                        break main;
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            } while (true);
        }
    } */

    /**
     * reset to an empty bitmap; result occupies as much space a newly
     * created bitmap.
     */
   /* public void clear() {
        highLowContainer = new RoaringArray(); // lose references
    } */

    @Override
    public LazyHierarRoaringBitmap clone() {
        try {
            final LazyHierarRoaringBitmap x = (LazyHierarRoaringBitmap) super.clone();
            x.highLowContainer = highLowContainer.clone();
            return x;
        } catch (final CloneNotSupportedException e) {
            throw new RuntimeException("shouldn't happen with clone", e);
        }
    }

    /**
     * Checks whether the value in included, which is equivalent to checking
     * if the corresponding bit is set (get in BitSet class).
     *
     * @param x integer value
     * @return whether the integer value is included.
     */
    @Override
    public boolean contains(final long x) {
    	final int pos = UtilLazyRoaring64bits.binarySearch((int)(x>>32), highLowContainer, 0, size);
    	return pos<0 ? false : highLowContainer[pos].contains((int)x);
    }

    /* /**
     * Deserialize (retrieve) this bitmap.
     * 
     * The current bitmap is overwritten.
     *
     * @param in the DataInput stream
     * @throws IOException Signals that an I/O exception has occurred.
     
    public void deserialize(DataInput in) throws IOException {
        this.highLowContainer.deserialize(in);
    } */

    @Override
    public boolean equals(Object o) {
        if (o instanceof LazyHierarRoaringBitmap) {
            final LazyHierarRoaringBitmap srb = (LazyHierarRoaringBitmap) o;
            return Arrays.equals(srb.highLowContainer, this.highLowContainer);
        }
        return false;
    }


    /*
     * Modifies the current bitmap by complementing the bits in the given
     * range, from rangeStart (inclusive) rangeEnd (exclusive).
     *
     * @param rangeStart inclusive beginning of range
     * @param rangeEnd   exclusive ending of range
    
    public void flip(final int rangeStart, final int rangeEnd) {
        if (rangeStart >= rangeEnd)
            return; // empty range

        final short hbStart = Util.highbits(rangeStart);
        final short lbStart = Util.lowbits(rangeStart);
        final short hbLast = Util.highbits(rangeEnd - 1);
        final short lbLast = Util.lowbits(rangeEnd - 1);

        final int max = Util.toIntUnsigned(Util.maxLowBit());
        for (short hb = hbStart; hb <= hbLast; ++hb) {
            // first container may contain partial range
            final int containerStart = (hb == hbStart) ? Util.toIntUnsigned(lbStart) : 0;
            // last container may contain partial range
            final int containerLast = (hb == hbLast) ? Util.toIntUnsigned(lbLast) : max;
            final int i = highLowContainer.getIndex(hb);

            if (i >= 0) {
                final Container c = highLowContainer.getContainerAtIndex(i).inot(
                                containerStart, containerLast);
                if (c.getCardinality() > 0)
                    highLowContainer.setContainerAtIndex(i, c);
                else
                    highLowContainer.removeAtIndex(i);
            } else {
                highLowContainer.insertNewKeyValueAt(-i - 1,hb, Container.rangeOfOnes(
                        containerStart, containerLast)
                );
            }
        }
    }*/

    /**
     * Returns the number of distinct integers added to the bitmap (e.g.,
     * number of bits set).
     *
     * @return the cardinality
     */
    public int getCardinality() {
        int card = 0;
        	for (int i = 0; i < size; i++) 
        			for(int j=0; j<highLowContainer[i].getNbChilds(); j++)
        				card += highLowContainer[i].getSecLevelArray()[j].getContainer().getCardinality();        
        return card;
    }

    /*
     * @return a custom iterator over set bits, the bits are traversed
     * in ascending sorted order
     
    public IntIterator getIntIterator() {
        return new RoaringIntIterator();
    }*/

    /*
     * @return a custom iterator over set bits, the bits are traversed
     * in descending sorted order
     *
    public IntIterator getReverseIntIterator() {
        return new RoaringReverseIntIterator();
    }*/

    /*
     * Estimate of the memory usage of this data structure. This
     * can be expected to be within 1% of the true memory usage.
     *
     * @return estimated memory usage.
     *
    public int getSizeInBytes() {
        int size = 8;
        for (int i = 0; i < this.highLowContainer.size(); i++) {
            final Container c = this.highLowContainer.getContainerAtIndex(i);
            size += 2 + c.getSizeInBytes();
        }
        return size;
    }*/

    @Override
    public int hashCode() {
        	int hashvalue = 0;
        	for(int k = 0; k < size; ++k)
        		hashvalue = 31 * hashvalue + this.highLowContainer[k].hashCode();
        	return hashvalue;
    }

    /**
     * iterate over the positions of the true values.
     *
     * @return the iterator
     */
    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            private int hs = 0;

            private ShortIterator iter;

            private int pos = 0;

            private int x;

            @Override
            public boolean hasNext() {
                return pos < 1<<16;//LazyHierarRoaringBitmap.this.highLowContainer.getChilds().length;//size();
            }

            /*private Iterator<Integer> init() {
                if (pos < LazyHierarRoaringBitmap.this.highLowContainer.size()) {
                    iter = LazyHierarRoaringBitmap.this.highLowContainer.getContainerAtIndex(pos).getShortIterator();
                    hs = Util.toIntUnsigned(LazyHierarRoaringBitmap.this.highLowContainer.getKeyAtIndex(pos)) << 16;
                }
                return this;
            }*/

            @Override
            public Integer next() {
                x = UtilLazyRoaring64bits.toIntUnsigned(iter.next()) | hs;
                if (!iter.hasNext()) {
                    ++pos;
                    //init();
                }
                return x;
            }

            @Override
            public void remove() {
                if ((x & hs) == hs) {// still in same container
                    iter.remove();
                } else {
                    //LazyHierarRoaringBitmap.this.remove(x);
                }
            }
            
        };//.init();
    }

    /*
     * Checks whether the bitmap is empty.
     * 
     * @return true if this bitmap contains no set bit
     
    public boolean isEmpty() {
    	return highLowContainer.size() == 0;
    }*/


    /*
     * In-place bitwise OR (union) operation. The current bitmap is
     * modified.
     *
     * @param x2 other bitmap
     
    public void or(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();
        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);

            while (true) {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    highLowContainer.insertNewKeyValueAt(pos1, s2, x2.highLowContainer.getContainerAtIndex(pos2)
                    );
                    pos1++;
                    length1++;
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    this.highLowContainer.setContainerAtIndex(pos1, highLowContainer.getContainerAtIndex(
                                    pos1).ior(x2.highLowContainer.getContainerAtIndex(pos2))
                    );
                    pos1++;
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2)) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            }
        }
        if (pos1 == length1) {
            highLowContainer.appendCopy(x2.highLowContainer, pos2, length2);
        }
    }*/

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        //this.highLowContainer.readExternal(in);
    }

    /**
     * If present remove the specified integers (effectively, sets its bit
     * value to false)
     *
     * @param x integer value representing the index in a bitmap.
     */
    public boolean remove(final long x) {
        final long hb = (int) (x>>>32);
        final int i = getIndex(hb);
        if (i < 0)
            return false;
        if(highLowContainer[i].isShared())
        	highLowContainer[i]= highLowContainer[i].clone();
        boolean b = highLowContainer[i].remove((int) x);
        if (b && highLowContainer[i].getNbChilds() == 0)
            removeAtIndex(i);
        return b;
    }
    
    public void removeAtIndex(int i) {
    	System.arraycopy(highLowContainer, i+1, highLowContainer, i, size-i-1);
    }
    
    public int getIndex(long hb){
    	int pos = UtilLazyRoaring64bits.binarySearch(hb, highLowContainer, 0, size);
    	return pos;
    }

    /*
     * Serialize this bitmap.
     * 
     * The current bitmap is not modified.
     *
     * @param out the DataOutput stream
     * @throws IOException Signals that an I/O exception has occurred.
     *
    public void serialize(DataOutput out) throws IOException {
        //this.highLowContainer.serialize(out);
    }*/

    /*
     * Report the number of bytes required to serialize this bitmap.
     * This is the number of bytes written out when using the serialize
     * method. When using the writeExternal method, the count will be
     * higher due to the overhead of Java serialization.
     *
     * @return the size in bytes
     *
    public int serializedSizeInBytes() {
        return this.highLowContainer.serializedSizeInBytes();
    }*/
    
    /*
     * Create a new Roaring bitmap containing at most maxcardinality integers.
     * 
     * @param maxcardinality maximal cardinality
     * @return a new bitmap with cardinality no more than maxcardinality
     *
    public LazyHierarRoaringBitmap limit(int maxcardinality) {
        LazyHierarRoaringBitmap answer = new LazyHierarRoaringBitmap();
        int currentcardinality = 0;        
        for (int i = 0; (currentcardinality < maxcardinality) && ( i < this.highLowContainer.size()); i++) {
            Container c = this.highLowContainer.getContainerAtIndex(i);
            if(c.getCardinality() + currentcardinality <= maxcardinality) {
               answer.highLowContainer.appendCopy(this.highLowContainer, i);
               currentcardinality += c.getCardinality();
            }  else {
                int leftover = maxcardinality - currentcardinality;
                Container limited = c.limit(leftover);
                answer.highLowContainer.append(this.highLowContainer.getKeyAtIndex(i),limited );
                break;
            }
        }
        return answer;
    }*/

    /**
     * Return the set values as an array. The integer
     * values are in sorted order.
     *
     * @return array representing the set values.
     */
    public Object[] toArray() {
    	ArrayList<Long> longs = new ArrayList<Long>();    	
    	for(int i=0; i<size; i++) {
         	long firstl = UtilLazyRoaring64bits.intToLongUnsigned(highLowContainer[i].getValue());
         	for(int j=0; j<highLowContainer[i].getNbChilds(); j++) {
         		long secl = UtilLazyRoaring64bits.shortToLongUnsigned(highLowContainer[i].getSecLevelArray()[j].getValue());
         		ShortIterator sit = highLowContainer[i].getSecLevelArray()[j].getContainer().getShortIterator();
         		while(sit.hasNext())
         			longs.add(firstl<<32|secl<<16|UtilLazyRoaring64bits.toIntUnsigned(sit.next()));
         	}
         }
    	Object[] a = longs.toArray();
    	Arrays.sort(a);
        return a;
    }

    /**
     * A string describing the bitmap.
     *
     * @return the string
     */
    @Override
    public String toString() {
    	 final StringBuffer answer = new StringBuffer();
         final StringBuffer firstLevel = new StringBuffer();
         final StringBuffer secLevel = new StringBuffer();
         final StringBuffer containers = new StringBuffer();
         
         firstLevel.append("{");
         for(int i=0; i<highLowContainer.length; i++) {
         	firstLevel.append(UtilLazyRoaring64bits.intToLongUnsigned(highLowContainer[i].getValue())+", ");
         	secLevel.append("{");
         	for(int j=0; j<highLowContainer[i].getSecLevelArray().length; j++) {
         		secLevel.append(UtilLazyRoaring64bits.shortToLongUnsigned(highLowContainer[i].getSecLevelArray()[j].getValue())+", ");
         		containers.append("{");
         		ShortIterator sit = highLowContainer[i].getSecLevelArray()[j].getContainer().getShortIterator();
         		while(sit.hasNext())
         			containers.append(UtilLazyRoaring64bits.toIntUnsigned(sit.next())+", ");
         		containers.append("} ");
         	}
         	secLevel.append("} ");
         }
         firstLevel.append("} ");
         
         answer.append(firstLevel.toString()+"\n"+secLevel.toString()+"\n"+containers.toString());
        
        return answer.toString();
    }

	@Override
	public IntIterator getIntIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int serializedSizeInBytes() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public IntIterator getReverseIntIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSizeInBytes() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}	

	@Override
	public int rank(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ImmutableBitmapDataProvider limit(int x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int select(int j) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void serialize(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

    /*
     * Recover allocated but unused memory.
     
    public void trim() {
        for (int i = 0; i < this.highLowContainer.size(); i++) {
            this.highLowContainer.getContainerAtIndex(i).trim();
        }
    } */
    
    /*
     * In-place bitwise XOR (symmetric difference) operation. The current
     * bitmap is modified.
     *
     * @param x2 other bitmap
     
    public void xor(final LazyHierarRoaringBitmap x2) {
        int pos1 = 0, pos2 = 0;
        int length1 = highLowContainer.size();
        final int length2 = x2.highLowContainer.size();

        main:
        if (pos1 < length1 && pos2 < length2) {
            short s1 = highLowContainer.getKeyAtIndex(pos1);
            short s2 = x2.highLowContainer.getKeyAtIndex(pos2);

            while (true) {
                if (s1 < s2) {
                    pos1++;
                    if (pos1 == length1) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                } else if (s1 > s2) {
                    highLowContainer.insertNewKeyValueAt(pos1, s2, x2.highLowContainer.getContainerAtIndex(pos2));
                    pos1++;
                    length1++;
                    pos2++;
                    if (pos2 == length2) {
                        break main;
                    }
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                } else {
                    final Container c = highLowContainer.getContainerAtIndex(pos1).ixor(
                            x2.highLowContainer.getContainerAtIndex(pos2));
                    if (c.getCardinality() > 0) {
                        this.highLowContainer.setContainerAtIndex(pos1, c);
                        pos1++;
                    } else {
                        highLowContainer.removeAtIndex(pos1);
                        --length1;
                    }
                    pos2++;
                    if ((pos1 == length1) || (pos2 == length2)) {
                        break main;
                    }
                    s1 = highLowContainer.getKeyAtIndex(pos1);
                    s2 = x2.highLowContainer.getKeyAtIndex(pos2);
                }
            }
        }
        if (pos1 == length1) {
            highLowContainer.appendCopy(x2.highLowContainer, pos2, length2);
        }
    }*/


    /*private final class RoaringIntIterator implements IntIterator {
        private int hs = 0;

        private ShortIterator iter;

        private int pos = 0;

        private int x;
        
        private RoaringIntIterator() {
            nextContainer();
        }

        @Override
        public boolean hasNext() {
            return pos < LazyHierarRoaringBitmap.this.highLowContainer.size();
        }

        private void nextContainer() {
            if (pos < LazyHierarRoaringBitmap.this.highLowContainer.size()) {
                iter = LazyHierarRoaringBitmap.this.highLowContainer.getContainerAtIndex(pos).getShortIterator();
                hs = Util.toIntUnsigned(LazyHierarRoaringBitmap.this.highLowContainer.getKeyAtIndex(pos)) << 16;
            }
        }

        @Override
        public int next() {
            x = Util.toIntUnsigned(iter.next()) | hs;
            if (!iter.hasNext()) {
                ++pos;
                nextContainer();
            }
            return x;
        }

        @Override
        public IntIterator clone() {
            try {
                RoaringIntIterator x = (RoaringIntIterator) super.clone();
                x.iter =  this.iter.clone();
                return x;
            } catch (CloneNotSupportedException e) {
                return null;// will not happen
            }
        }

    } */

   /* private final class RoaringReverseIntIterator implements IntIterator {

        int hs = 0;
        ShortIterator iter;
        // don't need an int because we go to 0, not Short.MAX_VALUE, and signed shorts underflow well below zero
        short pos = (short) (LazyHierarRoaringBitmap.this.highLowContainer.size() - 1);

        private RoaringReverseIntIterator() {
            nextContainer();
        }

        @Override
        public boolean hasNext() {
            return pos >= 0;
        }

        private void nextContainer() {
            if (pos >= 0) {
                iter = LazyHierarRoaringBitmap.this.highLowContainer.getContainerAtIndex(pos).getReverseShortIterator();
                hs = Util.toIntUnsigned(LazyHierarRoaringBitmap.this.highLowContainer.getKeyAtIndex(pos)) << 16;
            }
        }

        @Override
        public int next() {
            final int x = Util.toIntUnsigned(iter.next()) | hs;
            if (!iter.hasNext()) {
                --pos;
                nextContainer();
            }
            return x;
        }

        @Override
        public IntIterator clone() {
            try {
                RoaringReverseIntIterator clone = (RoaringReverseIntIterator) super.clone();
                clone.iter =  this.iter.clone();
                return clone;
            } catch (CloneNotSupportedException e) {
                return null;// will not happen
            }
        }

    } */
}





